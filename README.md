# qtc.jl

    qtc(params)

Calculate corrected QT interval.

## Arguments

- `params::Vector{String}`:  speed `s`: default is 25 (mm/s); no need to not provide units, QT `qt`: in seconds (s or sec), milliseconds (ms or msec), millimeters (mm); units have to specified, RR `rr`: in seconds (s or sec), milliseconds (ms or sec), millimeters (mm); units have to specified, HR `hr`: in bpm; no need to not provide units, e.g. `qt=12mm, rr=18mm, hr=60, s=50`

## Returns

Named tuple containing:
- `bazett::Float64`
- `fridericia::Float64`
- `hodges::Float64`
- `sagie::Float64`

## Notes

- Bazett: `QTc = QT / √RR`, source: Bazett HC (1920) An analysis of the time-relations of electrocardiograms. Heart 7:353-370.
- Fridericia: `QTc = QT / ∛RR`, source: Fridericia LS (1920) Die Systolendauer im Elektrokardiogramm bei normalen Menschen und bei Herzkranken. [The duration of systole in the electrocardiogram of normal subjects and of patients with heart disease.] Acta Medica Scandinavica 53:469–486.
- Hodges: `QTc = QT + 0.00175 × (HR - 60)`, source: Hodges M, Salerno D, Erlien D (1983) Bazett's QT correction reviewed. Evidence that a linear QT correction for heart is better. J Am Coll Cardiol 1:694.
- Sagie: `QTc = QT + 0.154 × (1  - RR)`, source: Sagie A, Larson MG, Goldberg RJ, Bengtson JR, Levy D (1992) An improved method for adjusting the QT interval for heart rate (the Framingham Heart Study). Am J Cardiol 70:797–801.

## License

This software is licensed under [The 2-Clause BSD License](LICENSE).