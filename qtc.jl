"""
    qtc(params)

Calculate corrected QT interval.

# Arguments

- `params::Vector{String}`: e.g. `qt=12mm, rr=18mm, hr=60, s=50`
    - `s`: speed, default is 25 (mm/s); no need to provide units
    - `qt`: QT in seconds (s or sec), milliseconds (ms or msec), millimeters (mm); units have to specified
    - `rr`: RR in seconds (s or sec), milliseconds (ms or sec), millimeters (mm); units have to specified
    - `hr`: HR in bpm; no need to provide units

# Returns:

Named tuple containing:
- `bazett::Float64`
- `fridericia::Float64`
- `hodges::Float64`
- `sagie::Float64`

# Notes

- Bazett: `QTc = QT / √RR`, source: Bazett HC (1920) An analysis of the time-relations of electrocardiograms. Heart 7:353-370.
- Fridericia: `QTc = QT / ∛RR`, source: Fridericia LS (1920) Die Systolendauer im Elektrokardiogramm bei normalen Menschen und bei Herzkranken. [The duration of systole in the electrocardiogram of normal subjects and of patients with heart disease.] Acta Medica Scandinavica 53:469–486.
- Hodges: `QTc = QT + 0.00175 × (HR - 60)`, source: Hodges M, Salerno D, Erlien D (1983) Bazett's QT correction reviewed. Evidence that a linear QT correction for heart is better. J Am Coll Cardiol 1:694.
- Sagie: `QTc = QT + 0.154 × (1  - RR)`, source: Sagie A, Larson MG, Goldberg RJ, Bengtson JR, Levy D (1992) An improved method for adjusting the QT interval for heart rate (the Framingham Heart Study). Am J Cardiol 70:797–801.
"""
function qtc(params::Vector{String})

    # default speed (in mm/s)
    s = 25

    # values in case not provided as arguments
    qt = 0
    rr = 0
    hr = 0
    qt_units = nothing
    rr_units = nothing

    # interpret arguments
    for idx in 1:length(params)
        if occursin(r"s\=(\d+)", params[idx])
            m = match(r"s\=(\d+)", params[idx]).captures
            s = parse(Float64, m[1])
        elseif occursin(r"hr=(\d+)", params[idx])
            m = match(r"hr\=(\d+)", params[idx]).captures
            hr = parse(Float64, m[1])
        elseif occursin(r"(.[a-z])\=(\d+\.\d+)(mm|ms|s)", params[idx])
            m = match(r"(.[a-z])\=(\d+\.\d+)(mm|ms|s)", params[idx]).captures
            if m[1] == "qt"
                qt = parse(Float64, m[2])
                qt_units=Symbol(m[3])
            elseif m[1] == "rr"
                rr = parse(Float64, m[2])
                rr_units=Symbol(m[3])
            end
        elseif occursin(r"(.[a-z])\=(\d+)(mm|ms|s)", params[idx])
            m = match(r"(.[a-z])\=(\d+)(mm|ms|s)", params[idx]).captures
            if m[1] == "qt"
                qt = parse(Float64, m[2])
                qt_units=Symbol(m[3])
            elseif m[1] == "rr"
                rr = parse(Float64, m[2])
                rr_units=Symbol(m[3])
            end
        end
    end

    # convert values to s
    qt_units === :mm && (qt = qt / s)
    rr_units === :mm && (rr = rr / s)
    qt_units === :ms && (qt = qt / 1000) 
    rr_units === :ms && (rr = rr / 1000)

    hr != 0 && rr == 0 && (rr = 60 / hr)
    rr != 0 && hr == 0 && (hr = round(60 / rr))

    # calculate QTc
    qt >= rr && throw(ArgumentError("QT ($qt s) must be < RR ($rr s)."))
    b = round(1000 * qt / sqrt(rr))
    f = round(1000 * qt / cbrt(rr))
    s = round(1000 * qt + 0.154 * (1  - rr))
    h = round(1000 * qt + 0.00175 * (hr - 60))
    println("$(rpad("HR:", 16))$hr bpm")
    println("$(rpad("RR:", 16))$rr s")
    println("$(rpad("QT:", 16))$qt s")
    println()
    println("$(rpad("QTc Bazett:", 16))$b ms")
    println("$(rpad("QTc Fredericia:", 16))$f ms")
    println("$(rpad("QTc Sagie:", 16))$s ms")
    println("$(rpad("QTc Hodges:", 16))$h ms")
    println()

    # return available QTc values
    return (bazett=b, fridericia=f, sagie=s, hodges=h)
end